#define CLCD_ADDRESS  0x27
void clcd2004_init();
void clcd2004_hello();
void LCD_wData(char8 dat);
void LCD_wString(char *str);
int clcd_string(u32 i2c_driver, u8 i2c_addr, char8 wrdata);

#define PCF8574_PIN_RW       0x02
#define PCF8574_PIN_P3       0x08
#define PCF8574_PIN_DB4      0x10
#define PCF8574_PIN_DB5      0x20
#define PCF8574_PIN_DB6      0x40
#define PCF8574_PIN_DB7      0x80
#define HD44780_BUSY_FLAG    HD44780_DB7
#define HD44780_INIT_SEQ     0x30
#define HD44780_DISP_CLEAR   0x01
#define HD44780_DISP_OFF     0x08
#define HD44780_DISP_ON      0x0C
#define HD44780_CURSOR_ON    0x0E
#define HD44780_CURSOR_BLINK 0x0F
#define HD44780_RETURN_HOME  0x02
#define HD44780_ENTRY_MODE   0x06
#define HD44780_4BIT_MODE    0x20
#define HD44780_8BIT_MODE    0x30
#define HD44780_2_ROWS       0x08
#define HD44780_FONT_5x8     0x00
#define HD44780_FONT_5x10    0x04
#define HD44780_POSITION     0x80
#define HD44780_SHIFT        0x10
#define HD44780_CURSOR       0x00
#define HD44780_DISPLAY      0x08
#define HD44780_LEFT         0x00
#define HD44780_RIGHT        0x04
#define HD44780_ROWS         2
#define HD44780_COLS         16
#define HD44780_ROW1_START   0x00
#define HD44780_ROW1_END     HD44780_COLS
#define HD44780_ROW2_START   0x40
#define HD44780_ROW2_END     HD44780_COLS
#define PCF8574_PIN_EN1      0x04
#define PCF8574_PIN_EN0      0x00
#define BACKLIGHT 			 0x08
#define PCF8574_PIN_RS       0x01

