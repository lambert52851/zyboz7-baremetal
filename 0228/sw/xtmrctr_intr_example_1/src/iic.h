#define IIC_BASE_ADDRESS	XPAR_IIC_0_BASEADDR






int LowLevelTempSensorExample(u32 IicBaseAddress,
				u8 TempSensorAddress,
				u8 *TemperaturePtr);

int write_i2c_reg(u32 i2c_driver, u8 i2c_addr, u8 wrdata);
int write_i2c_reg_dual(u32 i2c_driver, u8 i2c_addr, u8 wrdata);
