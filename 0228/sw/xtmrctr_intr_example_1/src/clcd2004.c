#include "xiic_l.h"
#include "clcd2004.h"
#include "iic.h"
#include "sleep.h"

//bit [7:0]
//DB7 DB6 DB5 DB4 RL EN RW RS

//RL은 전원이므로 항상 1임.
//RS=1 4BIT, -> RS=0 4BIT 하면 8BIT 값 써짐

//RW=0(읽기,쓰기 설정, 0은 쓰기)
//I/D=1 incremental

//모든 값에 0x08 or 연산
u8 RS_LOW = 0x00;
u8 RS_HIGH = 0x01;
u8 RW = 0x02;
u8 EN = 0x04;


void clcd2004_init()
{
		write_i2c_reg(IIC_BASE_ADDRESS, CLCD_ADDRESS, PCF8574_PIN_DB4 |PCF8574_PIN_DB5);
		usleep(5000); // 5ms sleep
		write_i2c_reg(IIC_BASE_ADDRESS, CLCD_ADDRESS, PCF8574_PIN_DB4 |PCF8574_PIN_DB5);
		usleep(500); //100us sleep
		write_i2c_reg(IIC_BASE_ADDRESS, CLCD_ADDRESS, PCF8574_PIN_DB4 |PCF8574_PIN_DB5);
		usleep(500); //100us sleep

		write_i2c_reg(IIC_BASE_ADDRESS, CLCD_ADDRESS, PCF8574_PIN_DB5);
		usleep(500); //100us sleep


		write_i2c_reg_dual(IIC_BASE_ADDRESS, CLCD_ADDRESS, PCF8574_PIN_DB5 | HD44780_2_ROWS);//0x28
		usleep(500); //100us sleep
		write_i2c_reg_dual(IIC_BASE_ADDRESS, CLCD_ADDRESS, HD44780_DISP_OFF);//2 0x08
		usleep(500); //100us sleep
		write_i2c_reg_dual(IIC_BASE_ADDRESS, CLCD_ADDRESS, HD44780_DISP_CLEAR);// 0x01
		usleep(500); //100us sleep
		write_i2c_reg_dual(IIC_BASE_ADDRESS, CLCD_ADDRESS, HD44780_RIGHT | HD44780_RETURN_HOME);// 0x06 : entry mode, right, i/d=1(2)
		usleep(500); //100us sleep
		write_i2c_reg_dual(IIC_BASE_ADDRESS, CLCD_ADDRESS, HD44780_CURSOR_BLINK);//0xf, display on, cursor on, blink on
		usleep(500); //100us sleep
		xil_printf("clcd init complete\r\n");
}


void LCD_wString(char *str){
	while(*str)
	LCD_wData(*str++);
}

void LCD_wData(char dat){
	clcd_string(IIC_BASE_ADDRESS, CLCD_ADDRESS, dat);
}

int clcd_string(u32 i2c_driver, u8 i2c_addr, char wrdata) {
//8BIT 데이터 전송용 EN1->0으로 바꿈. 8비트 전송
  u8 wrbuf[4];
  unsigned int wrcount;


  wrbuf[0] = (wrdata & 0xf0) | PCF8574_PIN_EN1 | BACKLIGHT | PCF8574_PIN_RS;
  wrbuf[1] = (wrdata & 0xf0) | PCF8574_PIN_EN0 | BACKLIGHT | PCF8574_PIN_RS;
  wrbuf[2] = (wrdata<<4 & 0xf0) | PCF8574_PIN_EN1 | BACKLIGHT | PCF8574_PIN_RS;
  wrbuf[3] = (wrdata<<4 & 0xf0) | PCF8574_PIN_EN0 | BACKLIGHT | PCF8574_PIN_RS;


  wrcount = XIic_Send(i2c_driver, i2c_addr, wrbuf, 4, XIIC_STOP);
  if (wrcount == 4)
	return 1;
  else {
	xil_printf("?I2C Send Error\r\n");
	return 0;
  }
}


int write_i2c_reg(u32 i2c_driver, u8 i2c_addr, u8 wrdata) {
//4bit 명령어용, 4비트 명령하고, EN 1->0으로 바꿈. 백라이트는 항상 켜져있도록 함.

  u8 wrbuf[1];
  unsigned int wrcount;

  wrbuf[0] = (wrdata & 0xf0) | PCF8574_PIN_EN1 | BACKLIGHT;
  wrbuf[1] = (wrdata & 0xf0) | PCF8574_PIN_EN0 | BACKLIGHT;


  wrcount = XIic_Send(i2c_driver, i2c_addr, &wrbuf[0], 2, XIIC_STOP);
  if (wrcount == 2)
	return 1;
  else {
	xil_printf("?I2C Send Error\r\n");
	return 0;
  }
}

int write_i2c_reg_dual(u32 i2c_driver, u8 i2c_addr, u8 wrdata) {
//8BIT 데이터 전송용 EN1->0으로 바꿈. 8비트 전송
  u8 wrbuf[4];
  unsigned int wrcount;


  wrbuf[0] = (wrdata & 0xf0) | PCF8574_PIN_EN1 | BACKLIGHT;
  wrbuf[1] = (wrdata & 0xf0) | PCF8574_PIN_EN0 | BACKLIGHT;
  wrbuf[2] = (wrdata<<4 & 0xf0) | PCF8574_PIN_EN1 | BACKLIGHT;
  wrbuf[3] = (wrdata<<4 & 0xf0) | PCF8574_PIN_EN0 | BACKLIGHT;


  wrcount = XIic_Send(i2c_driver, i2c_addr, wrbuf, 4, XIIC_STOP);
  if (wrcount == 4)
	return 1;
  else {
	xil_printf("?I2C Send Error\r\n");
	return 0;
  }
}
