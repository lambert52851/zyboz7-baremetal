#include "xil_types.h"
#include "xiic_l.h"



int LowLevelTempSensorExample(u32 IicBaseAddress,
				u8  TempSensorAddress,
				u8 *TemperaturePtr)
{
	int ByteCount;

	ByteCount = XIic_Recv(IicBaseAddress, TempSensorAddress,
				TemperaturePtr, 1, XIIC_STOP);



	return ByteCount;
}






