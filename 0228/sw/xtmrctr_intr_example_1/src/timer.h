#include "xiic_l.h"
#include "xgpio.h"
#include "xscugic.h"
#include "xparameters.h"

#define INTC		XScuGic
#define TMRCTR_DEVICE_ID	XPAR_TMRCTR_0_DEVICE_ID
#define TMRCTR_INTERRUPT_ID	XPAR_FABRIC_TMRCTR_0_VEC_ID
#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID
#define TIMER_CNTR_0	 0

#define INTC_HANDLER	XScuGic_InterruptHandler
#define RESET_VALUE	 0xFFF0BDC7 //10ms timer
#define GPIO_1_DEVICE  XPAR_GPIO_1_DEVICE_ID
/*
 * The following constant determines which timer counter of the device that is
 * used for this example, there are currently 2 timer counters in a device
 * and this example uses the first one, 0, the timer numbers are 0 based
 */

//TIMING_INTERVAL = (MAX_COUNT - TLRx + 2) x AXI_CLOCK_PERIOD
//100 MHz 1sec timer -> period 0xFA0A1EEB





int TmrCtrSetupIntrSystem(INTC *IntcInstancePtr,
				XTmrCtr *InstancePtr,
				u16 DeviceId,
				u16 IntrId,
				u8 TmrCtrNumber);

void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber);

int TmrCtrIntrExample(INTC *IntcInstancePtr,
			XTmrCtr *TmrCtrInstancePtr,
			u16 DeviceId,
			u16 IntrId,
			u8 TmrCtrNumber);


