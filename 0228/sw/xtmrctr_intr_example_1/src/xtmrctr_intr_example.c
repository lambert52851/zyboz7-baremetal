
/***************************** Include Files *********************************/

#include "xparameters.h"
#include "xtmrctr.h"
#include "xil_exception.h"
#include "xgpio.h"
#include "xscugic.h"
#include "xil_printf.h"
#include "timer.h"
#include "xiic_l.h"
#include "sleep.h"
#include "aht10.h"
#include "clcd2004.h"
/**************************** Type Definitions *******************************/


/***************** Macros (Inline Functions) Definitions *********************/


/************************** Function Prototypes ******************************/





/************************** Variable Definitions *****************************/
XScuGic InterruptController;  /* The instance of the Interrupt Controller */
XTmrCtr TimerCounterInst;   /* The instance of the Timer Counter */
XGpio GPIO1;
//함수와 전역변수는 그냥 가져올수있지만 define 및 구조체는 extern으로 가져와야함
/*
 * The following variables are shared between non-interrupt processing and
 * interrupt processing such that they must be global.
 */
/*****************************************************************************/
/**
* This function is the main function of the Tmrctr example using Interrupts.
*
* @param	None.
*
* @return	XST_SUCCESS to indicate success, else XST_FAILURE to indicate a
*		Failure.
*
* @note		None.
*
******************************************************************************/


/* The Instance of the GPIO Driver */
int main(void)
{

	int Status;
	XGpio_Initialize(&GPIO1, GPIO_1_DEVICE);

	clcd2004_init();
	Status = TmrCtrIntrExample(&InterruptController,
				  &TimerCounterInst,
				  TMRCTR_DEVICE_ID,
				  TMRCTR_INTERRUPT_ID,
				  TIMER_CNTR_0);
	//timer interrupt start
	if (Status != XST_SUCCESS) {
		xil_printf("Tmrctr interrupt Example Failed\r\n");
		return XST_FAILURE;
	}
	xil_printf("Successfully ran Tmrctr interrupt Example\r\n");




//	aht10_iic_measure();

	while(1)
	{

//		clcd2004_hello();
//		aht10_iic_measure();
//		LCD_wString("MINT BABO ");
	}



	return XST_SUCCESS;

}


/*****************************************************************************/
/**
* This function does a minimal test on the timer counter device and driver as a
* design example.  The purpose of this function is to illustrate how to use the
* XTmrCtr component.  It initializes a timer counter and then sets it up in
* compare mode with auto reload such that a periodic interrupt is generated.
*
* This function uses interrupt driven mode of the timer counter.
*
* @param	IntcInstancePtr is a pointer to the Interrupt Controller
*		driver Instance
* @param	TmrCtrInstancePtr is a pointer to the XTmrCtr driver Instance
* @param	DeviceId is the XPAR_<TmrCtr_instance>_DEVICE_ID value from
*		xparameters.h
* @param	IntrId is XPAR_<INTC_instance>_<TmrCtr_instance>_INTERRUPT_INTR
*		value from xparameters.h
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	XST_SUCCESS if the Test is successful, otherwise XST_FAILURE
*
* @note		This function contains an infinite loop such that if interrupts
*		are not working it may never return.
*
*****************************************************************************/

