#include "xil_types.h"
#include "xiic_l.h"
#include "iic.h"
#include "aht10.h"
#include "sleep.h"
#include "math.h"
#include "stdlib.h"
#include "clcd2004.h"
u32 aht10_tmp;
u8 AHT10_RecvBuffer[8];
//배열 초기화 시 {} 쓰면 편함
u8 AHT10_READ_BYTE_NUM = 8;
u8 AHT10_SEND_DATA_BUFFER;
u8 AHT10_RECV_DATA_BUFFER;
u8 AHT10_INIT = 0xe1;
u8 AHT10_TRIG = 0xac;
u8 AHT10_RESET = 0xba;
u8 AHT10_BUF =0x0;

char *char_TEXT = "TEMP IS           "; //18
char *char_aht10_tmp={0};
int n=0;
int aht10_tmp_value=0;

void aht10_iic_measure()
{
	XIic_Send(IIC_BASE_ADDRESS, AHT10_TMP_ADDRESS, &AHT10_INIT, 1, XIIC_STOP);
	//aht10 init E1
	usleep(100); //데이터시트에는 안나와있지만 일정 sleep이있어야 동작함.
	XIic_Send(IIC_BASE_ADDRESS, AHT10_TMP_ADDRESS, &AHT10_TRIG, 1, XIIC_STOP);
	//aht10 trigger measurement 0xac
	usleep(7500); //trigger 이후 75ms 이후에 measure해야 함
	XIic_Recv(IIC_BASE_ADDRESS, AHT10_TMP_ADDRESS, AHT10_RecvBuffer,AHT10_READ_BYTE_NUM, XIIC_STOP);
	for(n=0;n<AHT10_READ_BYTE_NUM;n++)
	{
	xil_printf("%x ",AHT10_RecvBuffer[n]);
	}
	xil_printf("\r\n");
	aht10_tmp=((AHT10_RecvBuffer[3] & 0x0f) << 16 | AHT10_RecvBuffer[4] << 8 | AHT10_RecvBuffer[5]);
	aht10_tmp_value=(aht10_tmp*200/pow(2,20))-50;
	//aht10_hum=(AHT10_RecvBuffer[1] << 16 | AHT10_RecvBuffer[2] <<8);// | (AHT10_RecvBuffer[3] << 4));
	//aht10_hum_value=(aht10_hum/pow(2,20));

//	char_aht10_tmp =itoa(aht10_tmp_value);
	itoa(aht10_tmp_value,char_aht10_tmp,10);//10진수로 변환
	xil_printf("temp is %d\r\n",aht10_tmp_value);

	LCD_wString(char_TEXT);
	LCD_wString(char_aht10_tmp);

	//xil_printf("humidity is %d\r\n",aht10_hum_value);

}

//void int_to_char_lcd(int aht10_tmp)
//{
//	char
//}

